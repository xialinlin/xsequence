package com.xuanner.seq;

import com.xuanner.seq.sequence.Sequence;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by xuan on 2018/1/10.
 */
public class RedisTest_Api extends BaseTest {

    private Sequence sequence;

    @Before
    public void setup() {
        sequence = getRedisSequence();
    }

    @Test
    public void test() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            System.out.println("++++++++++id:" + sequence.nextValue());
        }
        System.out.println("interval time:" + (System.currentTimeMillis() - start));
    }
}
